package com.example.federicoalnabulsi.animals.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.federicoalnabulsi.animals.Data.Pet;
import com.example.federicoalnabulsi.animals.UI.DetailActivity;
import com.example.federicoalnabulsi.animals.R;


import java.util.ArrayList;
import java.util.List;


public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder>{

    private List<Pet> mPets;
    private Context mContext;


    public CustomAdapter(ArrayList<Pet> pets, Context context) {
        mPets = pets;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position){
      holder.pName.setText(mPets.get(position).getName());
      holder.petImage.setImageResource(R.drawable.peterror);

     holder.petImage.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent intent = new Intent(mContext,DetailActivity.class);
              intent.putExtra("PET_ID", mPets.get(position).getId());
              mContext.startActivity(intent);
          }
      });



    }

    @Override
    public int getItemCount() {
        return mPets.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView pName;

        ImageView petImage;


        public ViewHolder(View itemView) {
            super(itemView);
            pName = itemView.findViewById(R.id.pet_name);
            petImage= itemView.findViewById(R.id.pet_image);

        }
    }



}
