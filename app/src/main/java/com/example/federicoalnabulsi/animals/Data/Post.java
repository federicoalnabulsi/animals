package com.example.federicoalnabulsi.animals.Data;

public class Post {

private String id;
private String tittle;
private String body;

    public Post(String id, String tittle, String body) {
        this.id = id;
        this.tittle = tittle;
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public String getTittle() {
        return tittle;
    }

    public String getBody() {
        return body;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
