package com.example.federicoalnabulsi.animals.Network;

import com.example.federicoalnabulsi.animals.Data.Pet;
import com.example.federicoalnabulsi.animals.Data.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface XmlPlaceHolderApi {

    @GET("findByStatus")
    Call<List<Pet>> getPets(@Query("status") String status);

    @GET("{petId}")
    Call<Pet> getOnePet(@Path("petId") String petId);



}
