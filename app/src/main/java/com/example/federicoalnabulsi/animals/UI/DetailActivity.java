package com.example.federicoalnabulsi.animals.UI;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.federicoalnabulsi.animals.Data.Pet;
import com.example.federicoalnabulsi.animals.UI.Fragment.PetFragment;
import com.example.federicoalnabulsi.animals.UI.Fragment.VibeFragment;
import com.example.federicoalnabulsi.animals.Network.XmlPlaceHolderApi;
import com.example.federicoalnabulsi.animals.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailActivity extends AppCompatActivity {

    public String petId;
    private Retrofit retrofit;
    public View vista;
    public TextView petName;
    public static Pet pet;
    public PetFragment petFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        petId= getIntent().getStringExtra("PET_ID");
        petName=findViewById(R.id.pet_name_detail);
        getPetApi(petId);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);



    }
    private void getPetApi(String petId){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://petstore.swagger.io/v2/pet/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        XmlPlaceHolderApi xmlPlaceHolderApi = retrofit.create(XmlPlaceHolderApi.class);
        Call<Pet> call = xmlPlaceHolderApi.getOnePet(petId);
        call.enqueue(new Callback<Pet>() {
            @Override
            public void onResponse(Call<Pet> call, Response<Pet> response) {
                if(!response.isSuccessful()) {
                    petName.setText("Code:" + response.code());
                    pet=null;
                    return;
                }

                setAdapters(response.body());

            }


            @Override
            public void onFailure(Call<Pet> call, Throwable t) {
                petName.setText(t.getMessage());



            }
        });

    }


    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;

            switch (item.getItemId()) {
                case R.id.nav_pet:
                    selectedFragment = petFragment;
                    break;
                case R.id.nav_vibes:
                    selectedFragment = new VibeFragment();
                    break;

            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragment).commit();
            return true;
        }


    };


private void setAdapters(Pet pet){
    petFragment=new PetFragment(pet);
    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,petFragment).commit();
}



}