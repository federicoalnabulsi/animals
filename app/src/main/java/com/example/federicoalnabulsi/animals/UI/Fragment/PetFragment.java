package com.example.federicoalnabulsi.animals.UI.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.federicoalnabulsi.animals.Data.Pet;
import com.example.federicoalnabulsi.animals.R;
import com.squareup.picasso.Picasso;

public class PetFragment extends Fragment {

     public Pet pet;
    public View vista1;
     public TextView petName1;
     public ImageView petImage;


     public PetFragment(){
         super();
     }
     @SuppressLint("ValidFragment")
     public PetFragment(Pet pet){
         super();
         this.pet=pet;
     }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        vista1=inflater.inflate(R.layout.fragment_pet,container,false) ;
        petImage=vista1.findViewById(R.id.pet_image);
        petName1=vista1.findViewById(R.id.pet_name_detail);
        if (pet != null) {
            Picasso.get()
                    .load(pet.getPhotosUrlList().get(0))
                    .error(R.drawable.peterror)
                    .into(petImage);

            petName1.setText(pet.getName());
        }
        else {
            petName1.setText("djhddjj");
        }

        return vista1;
    }



}
