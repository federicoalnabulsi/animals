package com.example.federicoalnabulsi.animals.UI.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.federicoalnabulsi.animals.Network.XmlPlaceHolderApi;
import com.example.federicoalnabulsi.animals.R;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VibeFragment extends Fragment {
     private Retrofit retro;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        retro=retrofitPetId();
        XmlPlaceHolderApi xmlPlaceHolderApi = retro.create(XmlPlaceHolderApi.class);


        return inflater.inflate(R.layout.fragement_vibe,container,false) ;
    }


    public Retrofit retrofitPetId(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://petstore.swagger.io/v2/pet/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;

    }
}
