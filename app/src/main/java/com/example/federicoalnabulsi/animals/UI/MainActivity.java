package com.example.federicoalnabulsi.animals.UI;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import com.example.federicoalnabulsi.animals.Adapter.CustomAdapter;
import com.example.federicoalnabulsi.animals.Data.Pet;
import com.example.federicoalnabulsi.animals.Network.XmlPlaceHolderApi;
import com.example.federicoalnabulsi.animals.R;
import java.util.ArrayList;
import java.util.List;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    public String status;
    private ArrayList<Pet> mPets;
    private RecyclerView recycleId;
    private ProgressBar progressBar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar=findViewById(R.id.progressBar);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://petstore.swagger.io/v2/pet/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        XmlPlaceHolderApi xmlPlaceHolderApi = retrofit.create(XmlPlaceHolderApi.class);
        status = "available";
        Call<List<Pet>> call = xmlPlaceHolderApi.getPets(status);

        mPets = new ArrayList<Pet>();
        call.enqueue(new Callback<List<Pet>>() {
            @Override
            public void onResponse(Call<List<Pet>> call, Response<List<Pet>> response) {
                if(!response.isSuccessful()) {
                    return;
                }
                List<Pet> pets = response.body();

                for (Pet pet : pets ){
                   mPets.add(pet);
                }
                setAdapter();
                progressBar.setVisibility(View.GONE);

            }

         @Override
            public void onFailure(Call<List<Pet>> call, Throwable t) {
             progressBar.setVisibility(View.GONE);


            }
        });


    }

private void setAdapter(){
    recycleId = findViewById(R.id.recycleID);
    recycleId.setLayoutManager(new GridLayoutManager(this,2));
    CustomAdapter adapter = new CustomAdapter(mPets,this);
    recycleId.setAdapter(adapter);

}



}


